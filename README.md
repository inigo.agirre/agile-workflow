# An agile workflow with GitLab
![Made-with-Markdown](https://img.shields.io/badge/agile-blue
)
![Made-with-Markdown](https://img.shields.io/badge/scrum-red
)
![Made-with-Markdown](https://img.shields.io/badge/issues-orange
)
![Made-with-Markdown](https://img.shields.io/badge/workflow-green
)

Este repositorio es un ejemplo de como implementar una **metodología de trabajo ágil**, mediante el uso de **SCRUM** con las herramientas proporcionadas por la plataforma **GitLab**. Esto incluye la creación del equipo de trabajo, la lectura de recursos relevantes sobre el uso de esta plataforma para desarrollo de software ágil, la mejora en la forma de organizar el trabajo.

## Definición

En este proyecto contamos con 3 integrantes cuyo rol es el de desarrolladores *software*. Como objetivo final, se plantea el desarrollo de una aplicación para la gestión de la logística de una empresa de fabricación de piezas.

Es de vital importancia contar con versionaes funcionales lo antes posible dadas las necesidades de los *stakeholders* que han invertido en el proyecto. Se propone la aplicación de SCRUM por ser una metodología de desarrollo ágil ampliamente documentada y que ha dado buenos resultados.

### Roles

Además de trabajar como desarrollador, cada miembro del equipo ha desempeñado un rol específico de SCRUM para obtener el máximo beneficio de la metodología:

- Mikel Aristu (**Scrum Master** / Desarrollador)
- Jon Andoni (**Product Owner** / Desarrollador)
- Iñigo Agirre (Desarrollador)

### Reuniones

Otro aspecto fundamental de SCRUM son las distintas reuniones que se realizan en momentos clave de cada Sprint de tal forma que se gestione correctamente los posibles riesgos del proyecto. 

- **Sprint Planning**: Antes de iniciar cada Sprint, se ha llevado a cabo una reunión para priorizar las historias de usuario y elegir el Sprint Backlog.
- **Daily Scrums**: Se ha hecho un seguimiento del Sprint con reuniones diarias para poner en común avances y problemas.
- **Sprint Review**: Se ha realizado al final de cada Sprint para analizar las tareas que se han cumplido y las que no.
- **Retrospective Meeting**: Ha permitido inspeccionar el equipo y plantear mejoras que se apliquen en el siguiente Sprint.


### Etiquetas

El Backlog se organiza en historias con etiquetas para observar el progreso del desarrollo:

- **Backlog**: Esta lista se comparte entre las Epics que han sido aceptadas y están listas para su desarrollo. Ayuda a que la transición sea realizada entre dos boards. Se coloca una etiqueta de "Borrador" cuando la "Historia" todavía se está escribiendo.

- **To Do**: Una cuestión -una historia, una tarea o un error- debe debatirse, estimarse y asignarse a una persona antes de pasar a "Por hacer". Es importante priorizarlo en función de las dependencias técnicas.

- **Doing**: Una incidencia que está en desarrollo por una persona. Debe haber un responsable por Tarea.

- **Test**: Una incidencia ha sido desarrollada, y está siendo revisada por otro miembro del equipo y finalmente por el Product Owner.
Closed: Una incidencia ha sido desarrollada y revisada tanto por otro miembro del equipo como por el Product Owner.

## Evidencias

A continuación, se muestra un tablero ágil simple que recoge las historias de usuario con una lista para cada una de las etiquetas mencionadas:


![Agile Board](./images/agile-board.gif)

Cada historia de usuario tiene una fecha para ser realizada, así como una estimación temporal del tiempo que se tardaría en completarla.

También se han creado tareas específicas para cada historia de usuario. Esto permite a los desarrolladores concretar detalles técnicos acerca de cómo lograr la implementación.

![Task Detail](./images/task-detail.png)

> **Nota:** No se ha podido realizar la creación de milestones y burdown chart, ya que se trata de características premium de GitLab.

## Reflexión

En general, la aplicación de la metodología SCRUM nos ha facilitado una toma de decisón rápida y efectiva. También cabe resaltar que las reuniones retrospectivas has servido para analizar puntos de riesgo y llevar a cabo cambios significativos en nuevas iteraciones, ayudando al cumpliento de los objetivos establecidos.

## Autores

- Mikel Aristu – [mikel.aristu@alumni.mondragon.edu](mailto:mikel.aristu@alumni.mondragon.edu)

- Jon Andoni Castillo – [jonandoni.castillo@alumni.mondragon.edu](mailto:jonandoni.castillo@alumni.mondragon.edu)

- Iñigo Agirre – [inigo.agirre@alumni.mondragon.edu](mailto:inigo.agirre@alumni.mondragon.edu)

### Colaboradores

- Urtzi Markiegi - [umarkiegi@mondragon.edu](mailto:umarkiegi@mondragon.edu)

##  Licencia
Este proyecto está licenciado bajo la licencia [MIT License](https://choosealicense.com/licenses/mit/).